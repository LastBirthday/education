package ru.education.model;

/**
 * Список предметов
 *
 * @author Denis Dmitrienko
 */
public enum Subject {
    /**
     * Математика
     */
    MATHS,

    /**
     * Физика
     */
    PHYSICS,

    /**
     * Информатика
     */
    INFORMATICS
}
