package ru.education.model;

import java.util.Date;

/**
 * Базовая информация о студенте
 *
 * @author Denis Dmitrienko
 */
public class Student {

    /**
     * Имя
     */
    private String name;

    /**
     * Фамилия
     */
    private String surname;

    /**
     * Отчество
     */
    private String middleName;

    /**
     * Дата рождения
     */
    private Date birthday;

    /**
     * Пол
     */
    private Gender gender;

    public enum Gender {
        /**
         * Мужчина
         */
        MALE,

        /**
         * Женщина
         */
        FEMALE;

        public static Gender from(String gender) {
            if (gender.equals("М")) {
                return MALE;
            }
            if (gender.equals("Ж")) {
                return FEMALE;
            }
            throw new IllegalStateException("Неизвестное значение пола: " + gender);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
