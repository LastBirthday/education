package ru.education.model;

import java.util.Arrays;

/**
 * Оценки по предмету
 *
 * @author Denis Dmitrienko
 */
public class SubjectScores {
    public static final int HIGHEST_SCORE = 5;

    /**
     * Предмет
     */
    private Subject subject;

    /**
     * Оценки по предмету
     */
    private int[] scores;

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int[] getScores() {
        return scores;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "SubjectScores{" +
                "subject=" + subject +
                ", scores=" + Arrays.toString(scores) +
                '}';
    }
}
