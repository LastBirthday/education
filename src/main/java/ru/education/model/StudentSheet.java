package ru.education.model;

import java.util.List;

/**
 * Студенческая ведомость
 *
 * @author Denis Dmitrienko
 */
public class StudentSheet {

    /**
     * Студенте
     */
    private Student student;

    /**
     * Группа
     */
    private String group;

    /**
     * Стипендия
     */
    private int studentship;

    /**
     * Оценки по предметам
     */
    private List<SubjectScores> subjectScores;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getStudentship() {
        return studentship;
    }

    public void setStudentship(int studentship) {
        this.studentship = studentship;
    }

    public List<SubjectScores> getSubjectScores() {
        return subjectScores;
    }

    public void setSubjectScores(List<SubjectScores> subjectScores) {
        this.subjectScores = subjectScores;
    }

    @Override
    public String toString() {
        return "StudentSheet{" +
                "student=" + student +
                ", group='" + group + '\'' +
                ", studentship=" + studentship +
                ", subjectScores=" + subjectScores +
                '}';
    }
}
