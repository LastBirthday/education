package ru.education.data;

/**
 * Входные данные
 *
 * @author Denis Dmitrienko
 */
public class InputData {
    /**
     * Разделитель между ФИО
     */
    public static final String FIO_DELIMITER = " ";

    /**
     * Рзаделитель между оценками
     */
    public static final String SCORES_DELIMITER = ", ";

    /**
     * Маппинг позиции заголовка к номеру колонки с данными
     */
    public enum Header {
        GROUP(0),
        FIO(1),
        BIRTHDAY(2),
        GENDER(3),
        SCORES_PHYSICS(4),
        SCORES_MATH(5),
        SCORES_INFORMATICS(6),
        STUDENTSHIP(7);

        private int position;

        Header(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

    public static final String[][] data = {
            {"401", "Беляев Владимир Николаевич", "1999-03-14", "М", "5, 4, 4, 4, 3, 5", "2, 3, 2", "5, 5", "0"},
            {"401", "Дмитриенко Денис Сергеевич", "1992-02-03", "М", "4, 3, 3, 5", "5, 4, 4, 5", "5, 5", "2500"},
            {"302", "Попугаев Попугай Попугайчикович", "2007-01-21", "М", "5, 5, 5, 5", "5, 5", "5, 5", "2500"},
            {"302", "Голубева Голубка Голубковичкина", "2012-10-11", "Ж", "4, 4, 5, 5", "4, 5, 5", "5, 5, 5", "2500"},
            {"401", "Голубев Андрей Дредович", "1991-06-07", "М", "5, 5, 5, 5, 5, 5, 5", "5, 5, 5, 5, 5", "5, 5, 5, 5", "2500"}
    };
}
