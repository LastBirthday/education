package ru.education;

import ru.education.data.InputData;
import ru.education.model.Student;
import ru.education.model.StudentSheet;
import ru.education.model.Subject;
import ru.education.model.SubjectScores;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Dmitrienko
 */
public class StudentSheetProcessor {
    private static final int STUDENTSHIP_RAISE_VALUE = 2000;

    public static void main(String[] args) {
        // Входные данные забиты в памяти. Берём из памяти.
        String[][] inputData = InputData.data;

        // Разбиваем входные данные по структурам.
        List<StudentSheet> studentSheets = parseInputDataToSheet(inputData);

        // Выполняем требования задания.
        raiseStudentshipForExcellentStudents(studentSheets);
    }

    private static void raiseStudentshipForExcellentStudents(List<StudentSheet> studentSheets) {
        for (int i = 0; i < studentSheets.size(); i++) {
            StudentSheet studentSheetRecord = studentSheets.get(i);

            boolean excellentStudent = checkScores(studentSheetRecord);

            if (excellentStudent) {
                int raisedStudentship = studentSheetRecord.getStudentship() + STUDENTSHIP_RAISE_VALUE;
                studentSheetRecord.setStudentship(raisedStudentship);
                System.out.println(studentSheetRecord);
            }
        }
    }

    private static boolean checkScores(StudentSheet studentSheetRecord) {
        for (int i = 0; i < studentSheetRecord.getSubjectScores().size(); i++) {
            SubjectScores subjectScores = studentSheetRecord.getSubjectScores().get(i);
            for (int j = 0; j < subjectScores.getScores().length; j++) {
                int [] scores = subjectScores.getScores();
                // Если хотя бы одна оценка в одном из предметов не 5, возвращаем false.
                if (scores[j] != SubjectScores.HIGHEST_SCORE) {
                    return false;
                }
            }
        }

        return true;
    }

    private static List<StudentSheet> parseInputDataToSheet(String[][] data) {
        List<StudentSheet> sheet = new ArrayList<>();

        for (int i = 0; i < data.length; i++) {
            StudentSheet sheetRecord = new StudentSheet();

            sheetRecord.setGroup(data[i][InputData.Header.GROUP.getPosition()]);

            Student student = new Student();
            student = parseAndSetStudentFIO(student, data[i][InputData.Header.FIO.getPosition()]);
            student.setBirthday(
                    Date.valueOf(data[i][InputData.Header.BIRTHDAY.getPosition()])
            );
            student.setGender(Student.Gender.from(data[i][InputData.Header.GENDER.getPosition()]));

            sheetRecord.setStudent(student);

            SubjectScores physicsScores = parseSubjectScore(Subject.PHYSICS, data[i][InputData.Header.SCORES_PHYSICS.getPosition()]);
            SubjectScores mathScores = parseSubjectScore(Subject.MATHS, data[i][InputData.Header.SCORES_MATH.getPosition()]);
            SubjectScores informaticsScores = parseSubjectScore(Subject.INFORMATICS, data[i][InputData.Header.SCORES_INFORMATICS.getPosition()]);

            List<SubjectScores> subjectScores = new ArrayList<>();
            subjectScores.add(physicsScores);
            subjectScores.add(mathScores);
            subjectScores.add(informaticsScores);

            sheetRecord.setSubjectScores(subjectScores);
            sheetRecord.setStudentship(
                    Integer.parseInt(data[i][InputData.Header.STUDENTSHIP.getPosition()])
            );

            sheet.add(sheetRecord);
        }

        return sheet;
    }

    // Считаем, что ФИО может состоять либо из 2 слов без отчества, либо из 3.
    private static Student parseAndSetStudentFIO(Student student, String fio) {
        String[] splitFio = fio.split(InputData.FIO_DELIMITER);
        if (splitFio.length < 2) {
            throw new IllegalStateException("В графе ФИО указано недопустимое значение. " +
                    "ФИО должно состоять минимум из двух слов разделённых пробелом.");
        }
        student.setSurname(splitFio[0]);
        student.setName(splitFio[1]);
        if (splitFio.length > 2) {
            student.setMiddleName(splitFio[2]);
        }
        return student;
    }

    private static SubjectScores parseSubjectScore(Subject subject, String scores) {
        SubjectScores subjectScores = new SubjectScores();
        subjectScores.setSubject(subject);
        String[] stringScores = scores.split(InputData.SCORES_DELIMITER);
        int[] intScores = new int[stringScores.length];
        for (int i = 0; i < stringScores.length; i++) {
            intScores[i] = Integer.parseInt(stringScores[i]);
        }
        subjectScores.setScores(intScores);

        return subjectScores;
    }
}
